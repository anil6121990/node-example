var app=angular.module('myApp',[]);

app.controller('ApCtrl', function($scope, $http){
	var refresh=function(){
   $http.get('/contactlist').then(function(response){
    $scope.contactlist=response.data;
    $scope.contact="";
  });
 };
 refresh();

 $scope.addcontact=function()
 {
   $http.post('/contactlist', $scope.contact).then(function(response){
     console.log(response);
     refresh();
   });
 };

 $scope.remove=function(id){
   console.log(id);

   $http.delete('/contactlist/' + id).then(function(response){
    refresh();
    console.log(response);
  });
 };

 $scope.edit=function(id)
 {
  console.log(id);
  $http.get('/contactlist/' + id).then(function(response){
   $scope.contact=response.data;
   console.log($scope.contact);
 });
};

$scope.update=function(id )
  { 
      console.log(id);
      $http.post('/contactlist/' + id,  $scope.contact).then(function(response){
      console.log(response);
      refresh();
      });


  };
});