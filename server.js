var express=require('express');
var app=express();
var mongojs=require('mongojs');
var db=mongojs('contactlist',['contactlist']);
var bodyparser=require('body-parser');
app.use(express.static(__dirname  + "/public"));

app.use(bodyparser.json());
app.get('/contactlist', function(req,res){
    console.log("get Request");
    db.contactlist.find(function(err, docs){
       console.log(docs);
       res.json(docs);
    });
});
app.post('/contactlist', function(req, res){
        console.log(req.body)
        db.contactlist.insert(req.body, function(err,docs){
               res.json(docs);
        });
});

app.delete('/contactlist/:id', function(req, res){
  var id=req.params.id;
  console.log(id);
 db.contactlist.remove({_id: mongojs.ObjectId(id)}, function(err, docs){
   res.json(docs);
 })
});

app.get('/contactlist/:id', function(req, res){
  var id=req.params.id;
  console.log(id);
  db.contactlist.findOne({_id: mongojs.ObjectId(id)}, function(err, docs){
   res.json(docs);
 })
});

app.post('/contactlist/:id', function(req, res){
      var id=req.params.id;
      console.log(id);
      db.contactlist.update({_id: mongojs.ObjectId(id)}, function(err, docs){
         res.json(docs);
      })
        
});

app.listen(2000);
console.log("server is running 2000");